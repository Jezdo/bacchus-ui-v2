'use strict';

angular.module('app.controllers', [])

# overall control
.controller('AppCtrl', [
    '$scope', '$rootScope'
    ($scope, $rootScope) ->
        $window = $(window)

        $scope.main =
            brand: 'Bacchus'

        backendApiURL = window.location.host
        # if we're running on localhost:9000
        if(backendApiURL.indexOf("localhost") > -1)
            backendApiURL = "192.168.137.185"
        # If not figure out host and use it as our backend (assuming client running on host :8080)
        else
            backendApiURL = backendApiURL.substring(0, backendApiURL.indexOf(':'));

        window.backendurl = "http://"+backendApiURL;
        console.log(window.backendurl)


        $scope.pageTransitionOpts = [
            name: 'Scale up'
            class: 'ainmate-scale-up'
        ,   
            name: 'Fade up'
            class: 'animate-fade-up'
        ,   
            name: 'Slide in from right'
            class: 'ainmate-slide-in-right'
        ,   
            name: 'Flip Y'
            class: 'animate-flip-y'
        ]

        $scope.admin =
            layout: 'wide'                                  # 'boxed', 'wide'
            menu: 'vertical'                                # 'horizontal', 'vertical'
            fixedHeader: true                               # true, false
            fixedSidebar: false                             # true, false
            pageTransition: $scope.pageTransitionOpts[0]    # unlimited, check out "_animation.scss"

        $scope.$watch('admin', (newVal, oldVal) ->
            # manually trigger resize event to force morris charts to resize, a significant performance impact, enable for demo purpose only
            # if newVal.menu isnt oldVal.menu || newVal.layout isnt oldVal.layout
            #      $window.trigger('resize')

            if newVal.menu is 'horizontal' && oldVal.menu is 'vertical'
                 $rootScope.$broadcast('nav:reset')
                 return
            if newVal.fixedHeader is false && newVal.fixedSidebar is true
                if oldVal.fixedHeader is false && oldVal.fixedSidebar is false
                    $scope.admin.fixedHeader = true 
                    $scope.admin.fixedSidebar = true 
                if oldVal.fixedHeader is true && oldVal.fixedSidebar is true
                    $scope.admin.fixedHeader = false 
                    $scope.admin.fixedSidebar = false 
                return
            if newVal.fixedSidebar is true
                $scope.admin.fixedHeader = true
            if newVal.fixedHeader is false 
                $scope.admin.fixedSidebar = false

            return
        , true)

        $scope.color =
            primary:    '#248AAF'
            success:    '#3CBC8D'
            info:       '#29B7D3'
            infoAlt:    '#666699'
            warning:    '#FAC552'
            danger:     '#E9422E'

])

.controller('HeaderCtrl', [
    '$scope'
    ($scope) ->
])

.controller('NavContainerCtrl', [
    '$scope'
    ($scope) ->


])
.controller('NavCtrl', [
    '$scope', 'taskStorage', 'filterFilter'
    ($scope, taskStorage, filterFilter) ->
        # init
        tasks = $scope.tasks = taskStorage.get()
        $scope.taskRemainingCount = filterFilter(tasks, {completed: false}).length

        $scope.$on('taskRemaining:changed', (event, count) ->
            $scope.taskRemainingCount = count
        )
])

.controller('DashboardCtrl', [
    '$scope', 'temperatureFactory', 'ledFactory', '$log', '$interval'
    ($scope, temperatureFactory, ledFactory, $log, $interval ) ->
        ##########################
        # LED MODULE
        ##########################

        # To be implemented, used for controlling multiple LEDs
        ledId = 0

        # Method to turn LED off
        ledTurnOff = ->
            ledFactory.turnOffLed(ledId).success(
                (response, status) ->
                    $log.debug "Turning on led"
                    # Change status
                    $scope.led.status = false

            ).error(
                (response, status) ->
                    $log.debug(response)
            )

        # Method to turn LED on
        ledTurnOn = ->
            ledFactory.turnOnLed(ledId).success(
                (response, status) ->
                      $log.debug "Turning on led"
                      # Change status
                      $scope.led.status = true

            ).error(
                (response, status) ->
                    $log.debug(response)
            )


        # Method to toggle LED, calling previous functions
        ledToggle = ->
            # If enabled, turn off
            if($scope.led.status)
                ledTurnOff()
                # If disabled, turn on
            else
                ledTurnOn()

        # LED Module settings
        ledStatus = false # Fetch LED status via Ajax first time

        # Pass LED object to view
        $scope.led = {
            status: ledStatus,
            toggle: ledToggle
        }

        ##########################
        # TEMPERATURE MODULE
        ##########################

        $scope.refreshTemperature = () ->
            $log.debug "Refreshing temperature: " + new Date()
            temperatureFactory.currentTemperature().success(
                (response, status) ->
                    temperatureDeg = response[0].temperature
                    temperature = temperatureFactory.temperatureAnalyze(temperatureDeg)
                    $scope.temperature = temperature
                    $log.debug temperature
            ).error(
                (response, status) ->
                    $log.debug(response.data)
            )

        # Initialize temperature
        temperatureRefreshInterval = 1 # In minutes
        $scope.refreshTemperature()
        $interval(->
                    $scope.refreshTemperature()
                   , temperatureRefreshInterval*60*1000)


])
