'use strict'

# Dependencies: jQuery, related jQuery plugins

angular.module('temperature', [])

.controller('TemperatureCtrl', [
    '$scope'
    ($scope) ->
      $scope.tags = ['foo', 'bar']
  ])

