'use strict'

angular.module('temperature')
.factory "temperatureFactory", [
  "$http"
  ($http) ->

    factory = {}

    # Token Refresh
    factory.currentTemperature = ->
      $http.get window.backendurl + "/temperature"

    factory.temperatureAnalyze = (temperature) ->
      console.log temperature
      description = ""
      icon = ""
      bgColor = ""

      temperatureSteps = {
        veryCold: 5, # t<=5
        cold: 15, # t<=15 & t>5
        chill: 21, # t>15 & t<=21
        normal: 25, # t>21 & t<=25
        warm: 30, # t>25 & t <=30
        hot: 35, # t>30 & t<=35
      #veryHot: t > temperatureSteps.hot
      }

      descriptions = {
        veryCold: "Very Cold", # t<=5
        cold: "Cold", # t<=15 & t>5
        chill: "Chill", # t>15 & t<=21
        normal: "Perfect", # t>21 & t<=25
        warm: "Warm", # t>25 & t <=30
        hot: "Hot", # t>30 & t<=35
        veryHot: "Very Hot" # t>35
      }

      if (temperature <= temperatureSteps.veryCold)
        description = descriptions.veryCold
        icon = "wi-day-sunny"
        bgColor = "veryCold"
      else if (temperature <= temperatureSteps.cold)
        description = descriptions.cold
        icon = "wi-day-sunny"
        bgColor = "cold"
      else if (temperature <= temperatureSteps.chill)
        description = descriptions.chill
        icon = "wi-day-sunny"
        bgColor = "chill"
      else if (temperature <= temperatureSteps.normal)
        description = descriptions.normal
        icon = "wi-day-sunny"
        bgColor = "normal"
      else if (temperature <= temperatureSteps.warm)
        description = descriptions.warm
        icon = "wi-day-sunny"
        bgColor = "warm"
      else if (temperature <= temperatureSteps.hot)
        description = descriptions.hot
        icon = "wi-day-sunny"
        bgColor = "hot"
      else
        description = descriptions.veryHot
        icon = "wi-day-sunny"
        bgColor = "veryHot"

      return {
        temperature: temperature,
        description: description,
        icon: icon,
        bgColor: bgColor,
        updated: new Date()
      }

    return factory
]