'use strict'
angular.module('led', [])

angular.module('led')
.factory "ledFactory", [
  "$http"
  ($http) ->

    factory = {}

    # LED Enable
    factory.turnOnLed = (ledId) ->
      $http.get window.backendurl + "/led" + "/" + ledId + "/enable"

    # LED Disable
    factory.turnOffLed = (ledId) ->
      $http.get window.backendurl + "/led" + "/" + ledId + "/disable"

    return factory
]